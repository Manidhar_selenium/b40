package com.rediff.tesitng.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class Common {
	public WebDriver driver = null;
	public Properties env = null;
	public String path = System.getProperty("user.dir");
	public static Logger logger=null;
	// @BeforeSuite
	public void loadFiles() throws IOException {
		 logger=LogManager.getLogger(Common.class.getName());
		
		InputStream f = new FileInputStream(path + "\\src\\main\\resources\\Environment.properties");
		// C:\Users\manid\eclipse-workspace\selenium\B40\rediff\src\main\resources\Environment.properties
		env = new Properties();
		env.load(f);
		logger.debug("Property Files loaded");
	}

	// @AfterSuite
	public void closeFiles() {
		env = null;
		logger.debug("Files connection is closed");
	}

	public void openBrowsers() {
		String browserName = env.getProperty("browser");

		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", path + "\\src\\main\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.get(env.getProperty("url"));
			logger.debug("Opened Chrome Browser and navigated to application");

		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", path + "\\src\\main\\resources\\geckodriver.exe");
			WebDriver driver = new FirefoxDriver();
			driver.get(env.getProperty("url"));
			logger.debug("Opened Firefox Browser and navigated to application");

		} else if (browserName.equalsIgnoreCase("opera")) {
			System.setProperty("webdriver.opera.driver", path + "\\src\\main\\resources\\operadriver.exe");
			WebDriver driver = new OperaDriver();
			driver.get(env.getProperty("url"));
			logger.debug("Opened Opera Browser and navigated to application");

		} else if (browserName.equalsIgnoreCase("edge")) {
			System.setProperty("webdriver.edge.driver", path + "\\src\\main\\resources\\msedgedriver.exe");
			WebDriver driver = new EdgeDriver();
			driver.get(env.getProperty("url"));
			logger.debug("Opened Edge Browser and navigated to application");

		} else {
			throw new Error("Cross check your browser name on environment.properties");
		}

	}

	public void closeBrowsers() {
		driver.close();
		logger.debug("Close the Browsers");

	}

	public void captureScreenshot() throws IOException {
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File destination = new File(path + "\\screenshots\\Screen.png");
		FileUtils.copyFile(source, destination);
		logger.debug("Captured Screenshot");

	}
}
