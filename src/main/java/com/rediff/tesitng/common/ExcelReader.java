package com.rediff.tesitng.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelReader extends Common {

	public Object[][] readExcel() throws EncryptedDocumentException, IOException {
		InputStream f = new FileInputStream(path + "\\src\\main\\resources\\TestData.xlsx");
		Workbook workbook = WorkbookFactory.create(f);
		Sheet sheet = workbook.getSheetAt(0);

		int lastRowNo = sheet.getLastRowNum();

		int totalRows = lastRowNo + 1;
		Row row = sheet.getRow(0);
		int totalColumns = row.getLastCellNum();

		//logger.debug("totalRows: " + totalRows);
		//logger.debug("totalColumns: " + totalColumns);

		Object o[][] = new Object[totalRows][totalColumns];
		// all rows
		for (int i = 0; i < totalRows; i++) {
			row = sheet.getRow(i);
			totalColumns = row.getLastCellNum();
			// all columns in a row
			for (int j = 0; j < totalColumns; j++) {
				Cell cell = row.getCell(j);
				String values = cell.getStringCellValue();
				o[i][j]=values;
			}
		}
		//logger.debug("Completed reading excel data and stored in to Object[][]");

		return o;
	}

	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		ExcelReader er = new ExcelReader();
		er.readExcel();
	}

}
