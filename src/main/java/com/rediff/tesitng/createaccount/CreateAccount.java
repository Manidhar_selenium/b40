package com.rediff.tesitng.createaccount;
import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.rediff.tesitng.common.Common;

public class CreateAccount extends Common{

	@BeforeMethod
	public void openBrowser() throws IOException {
		loadFiles();
		openBrowsers();
	}

	@AfterMethod
	public void closeBrowser() {
		closeBrowsers();
	}
// Test to validate create account
	@Test
	public void createAcountTest() throws InterruptedException, IOException {
		try {
		SoftAssert softAssert = new SoftAssert();
		driver.findElement(By.partialLinkText("Create a new ac")).click();
		driver.findElement(By.xpath("//td[contains(text(),'Full Name')]/parent::tr/child::td[3]/input"))
				.sendKeys("manidhar");
		driver.findElement(By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/child::td[3]/input[1]"))
				.sendKeys("manidhar");
		driver.findElement(By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/child::td[3]/input[2]"))
				.click();
		Thread.sleep(1500);
		String actualMessage = driver.findElement(By.xpath("//div[@id='check_availability']/font[1]/b")).getText();
		String expectedMessage = "ID that you are looking for is taken.";

		logger.debug(actualMessage);
		// Assert.assertTrue(actualMessage.contains(expectedMessage),
		// "Message is not properly shown check the flow manually");
		softAssert.assertTrue(actualMessage.contains(expectedMessage),
				"Message is not properly shown check the flow manually");
		logger.debug("After hard assertion");
		// softAssert.assertFalse(true,"Negative Testing failed");
		softAssert.assertAll();
		logger.debug("Verified Create account with existing email id");

		}catch(Exception e) {
			e.printStackTrace();
			captureScreenshot();
			logger.error(e.toString());
		}
	}
}
