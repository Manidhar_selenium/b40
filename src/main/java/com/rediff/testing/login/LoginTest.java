package com.rediff.testing.login;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.rediff.tesitng.common.Common;
import com.rediff.tesitng.common.ExcelReader;

public class LoginTest extends Common{
	@BeforeMethod
	public void openBrowser() throws IOException{
		loadFiles();
		openBrowsers();
	}
//Test case to validate invalid login
	@Test(priority = 1, dataProvider = "testData")
	public void invalidLogin(String un, String ps) throws IOException {
		try {
		SoftAssert s=new SoftAssert();
		driver.findElement(By.id("login1")).sendKeys(un);
		driver.findElement(By.id("password")).sendKeys(ps);
		driver.findElement(By.name("proceed")).click();
		
		String actualErrorMessage=driver.findElement(By.id("div_login_error")).getText();
		String expectedErrorMessage="Wrong username and password combination.";
		
		s.assertEquals(actualErrorMessage, expectedErrorMessage);
		s.assertAll();
		logger.debug("Verified Invalid Login Scenario with username: "+un+" and with password: "+ps);

		}catch(Exception e) {
			e.printStackTrace();
			captureScreenshot();
			logger.debug(e.toString());
		}
	}
	@Test(priority = 2, dependsOnMethods = "invalidLogin",enabled=true)
	public void verifyPageTitle() throws IOException{
		try {
		SoftAssert s=new SoftAssert();
		String actualTitle=driver.getTitle();
		String excpectedTitle="Rediffmail5454";
		s.assertEquals(actualTitle, excpectedTitle);
		s.assertAll();
		logger.debug("Verified page title");

		}catch(Exception e) {
			e.printStackTrace();
			captureScreenshot();
			logger.debug(e.toString());
		}
	}
	@AfterMethod
	public void closeBrowser(){
		closeBrowsers();
	}
	@DataProvider
	public Object[][] testData() throws EncryptedDocumentException, IOException{
		ExcelReader excelData=new ExcelReader();
		return excelData.readExcel();
	}

}
